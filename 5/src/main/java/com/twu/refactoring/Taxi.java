package com.twu.refactoring;

public class Taxi {
    private boolean airConditioned;
    private final int totalKms;
    private final boolean peakTime;

    public Taxi(boolean airConditioned, int totalKms, boolean peakTime) {
        this.airConditioned = airConditioned;
        this.totalKms = totalKms;
        this.peakTime = peakTime;
    }

    boolean isAirConditioned() {
        return airConditioned;
    }

    int getTotalKms() {
        return totalKms;
    }

    boolean isPeakTime() {
        return peakTime;
    }
}
