package com.twu.refactoring;

public class South extends Direction {

    South() {
        super('S');
    }

    @Override
    public Direction turnRight() {
        return new Direction('W');
    }

    @Override
    public Direction turnLeft() {
        return new Direction('E');
    }
}
