package com.twu.refactoring;

public class West extends Direction {

    West() {
        super('W');
    }

    @Override
    public Direction turnRight() {
        return new Direction('N');
    }

    @Override
    public Direction turnLeft() {
        return new Direction('S');
    }
}
