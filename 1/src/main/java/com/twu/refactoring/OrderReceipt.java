package com.twu.refactoring;

public class OrderReceipt {
    private Order order;

    public OrderReceipt(Order order) {
        this.order = order;
    }

    public String printReceipt() {
        StringBuilder output = new StringBuilder();

        output.append("======Printing Orders======\n");
        output.append(order.getCustomerName()).append(order.getCustomerAddress());

        double totalSalesTax = 0d;
        double total = 0d;
        for (LineItem lineItem : order.getLineItems()) {
            lineItem.appendLineItem(output);
            double salesTax = lineItem.calculateSalesTax();
            totalSalesTax += salesTax;
            total += lineItem.totalAmount() + salesTax;
        }
        output.append("Sales Tax").append('\t').append(totalSalesTax);
        output.append("Total Amount").append('\t').append(total);
        return output.toString();
    }
}
