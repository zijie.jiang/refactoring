package com.twu.refactoring;

public class LineItem {
	private String description;
	private double price;
	private int quantity;

	public LineItem(String description, double price, int quantity) {
		super();
		this.description = description;
		this.price = price;
		this.quantity = quantity;
	}

	private String getDescription() {
		return description;
	}

	private double getPrice() {
		return price;
	}

	private int getQuantity() {
		return quantity;
	}

    double totalAmount() {
        return price * quantity;
    }

	Double calculateSalesTax() {
		return totalAmount() * .10;
	}

	void appendLineItem(StringBuilder output) {
		output.append(getDescription()).append('\t')
				.append(getPrice())
				.append('\t')
				.append(getQuantity())
				.append('\t')
				.append(totalAmount())
				.append('\n');
	}
}
