package com.twu.refactoring;

public enum DateTypeEnum {

    YEAR("Year", 0, 4, 2000, 2012, 4),
    MONTH("Month", 5, 7, 1, 12, 2),
    DAY("Date", 8, 10, 1, 31, 2),
    HOUR("Hour", 11, 13, 0, 23, 2),
    MINUTE("Minute", 14, 16, 0, 59, 2);

    DateTypeEnum(String dateType, Integer startIndex, Integer endIndex, Integer lowerLimit, Integer upperLimit, Integer number) {
        this.dateType = dateType;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.lowerLimit = lowerLimit;
        this.upperLimit = upperLimit;
        this.number = number;
    }

    private String dateType;
    private Integer startIndex;
    private Integer endIndex;
    private Integer upperLimit;
    private Integer lowerLimit;
    private Integer number;

    public int get(String dateAndTimeString) {
        int time;
        try {
            String string = dateAndTimeString.substring(getStartIndex(), getEndIndex());
            time = Integer.parseInt(string);
        } catch (StringIndexOutOfBoundsException e) {
            throw new IllegalArgumentException(String.format("%s string is less than %d characters", getDateType(), getNumber()));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(String.format("%s is not an integer", getDateType()));
        }
        if (time < getLowerLimit() || time > getUpperLimit())
            throw new IllegalArgumentException(String.format("%s cannot be less than %d or more than %d",
                    getDateType(), getLowerLimit(), getUpperLimit()));
        return time;
    }

    public String getDateType() {
        return dateType;
    }

    public Integer getStartIndex() {
        return startIndex;
    }

    public Integer getEndIndex() {
        return endIndex;
    }

    public Integer getUpperLimit() {
        return upperLimit;
    }

    public Integer getLowerLimit() {
        return lowerLimit;
    }

    public Integer getNumber() {
        return number;
    }
}
