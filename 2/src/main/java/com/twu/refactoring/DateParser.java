package com.twu.refactoring;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class DateParser {
    private final String dateAndTimeString;
    private static final Map<String, TimeZone> KNOWN_TIME_ZONES = new HashMap<>();

    static {
        KNOWN_TIME_ZONES.put("UTC", TimeZone.getTimeZone("UTC"));
    }

    public DateParser(String dateAndTimeString) {
        this.dateAndTimeString = dateAndTimeString;
    }

    public Date parse() {
        int year, month, day, hour, minute;
        year = DateTypeEnum.YEAR.get(dateAndTimeString);
        month = DateTypeEnum.MONTH.get(dateAndTimeString);
        day = DateTypeEnum.DAY.get(dateAndTimeString);
        boolean withZ = dateAndTimeString.substring(11, 12).equals("Z");
        hour = withZ ? 0 : DateTypeEnum.HOUR.get(dateAndTimeString);
        minute = withZ ? 0 : DateTypeEnum.MINUTE.get(dateAndTimeString);

        Calendar calendar = getCalendar(year, month, day, hour, minute);
        return calendar.getTime();
    }

    private Calendar getCalendar(int year, int month, int day, int hour, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(KNOWN_TIME_ZONES.get("UTC"));
        calendar.set(year, month - 1, day, hour, minute, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }
}
