package com.twu.refactoring;

public class Childrens extends Movie {

    Childrens() {
    }

    @Override
    protected double calculateAmount(Rental rental) {
        double amount = 1.5;
        if (rental.getDaysRented() > 3)
            amount += (rental.getDaysRented() - 3) * 1.5;
        return amount;
    }
}
