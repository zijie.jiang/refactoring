package com.twu.refactoring;

public class NewRelease extends Movie {

    NewRelease() {
    }

    @Override
    protected double calculateAmount(Rental rental) {
        return rental.getDaysRented() * 3;
    }
}
