package com.twu.refactoring;

public class Regular extends Movie {

    Regular() {
    }

    @Override
    protected double calculateAmount(Rental rental) {
        double amount = 2;
        if (rental.getDaysRented() > 2)
            amount += (rental.getDaysRented() - 2) * 1.5;
        return amount;
    }
}
