package com.twu.refactoring;

public class Movie {
	public static final int CHILDRENS = 2;
	public static final int REGULAR = 0;
	public static final int NEW_RELEASE = 1;

	private String title;
	private int priceCode;

	Movie() {
	}

	public Movie(String title, int priceCode) {
		this.title = title;
		this.priceCode = priceCode;
	}

	int getPriceCode() {
		return priceCode;
	}

	public void setPriceCode(int arg) {
    	priceCode = arg;
	}

	String getTitle() {
		return title;
	}

	protected double calculateAmount(Rental rental) {
		return 0;
	}
}

