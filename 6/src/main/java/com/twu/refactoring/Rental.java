package com.twu.refactoring;

import static com.twu.refactoring.Movie.CHILDRENS;
import static com.twu.refactoring.Movie.NEW_RELEASE;
import static com.twu.refactoring.Movie.REGULAR;

public class Rental {

    private Movie movie;

    private int daysRented;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    int getDaysRented() {
        return daysRented;
    }

    Movie getMovie() {
        return movie;
    }

    double calculateMovieAmount() {
        if (getMovie().getPriceCode() == REGULAR) {
            return new Regular().calculateAmount(this);
        }
        if (getMovie().getPriceCode() == NEW_RELEASE) {
            return new NewRelease().calculateAmount(this);
        }
        if (getMovie().getPriceCode() == CHILDRENS) {
            return new Childrens().calculateAmount(this);
        }
        return 0;
    }

    int getFrequentRenterPoints() {
        return getMovie().getPriceCode() == NEW_RELEASE && getDaysRented() > 1 ? 2 : 1;
    }
}
